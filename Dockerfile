FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/projectx-0.0.1-SNAPSHOT-standalone.jar /projectx/app.jar

EXPOSE 8080

CMD ["java", "-jar", "/projectx/app.jar"]
