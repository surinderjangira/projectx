(ns authorization.group_permissions
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [authorization.entities :as authorization-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v])
  (:use [korma.core]))

(defn get-group-data
  [entities group-id]
  (select entities
          (where {:id group-id})
          (limit 1)))

(defn get-group-permissions
  "Get all permissions and access for the user"
  [entities access-level object company-id group-id]
  (select entities
         (fields :permission :access_level)
          (where {:company_id company-id
                  :group_id group-id
                  :access_level access-level
                  :permission object
                  :status "active"})
          (limit 1)))

(defn check-authorization
  [access-level object group-id]
  (let [group (first (get-group-data authorization-entities/group group-id))]
    (def company-id (:company_id group))
    (def group-name (:group_name group))

    (if (= group-name "suadmin")
      (do
        (ring-resp/response {:authorize true}))
      (do
        (let [resultSet (or (first (get-group-permissions authorization-entities/group_permission access-level object company-id group-id)) nil)]
          (if-not (empty? resultSet)
            (ring-resp/response {:authorize true})
            (ring-resp/response {:authorize false})))))))

