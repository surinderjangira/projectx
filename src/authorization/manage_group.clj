(ns authorization.manage_group
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [authorization.entities :as authorization-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]))

(defn update-record
  "Update vendor record in specified table"
  [entities group-id company-id group-name deleted-status status]
  (def where-condition {:group_name ['ilike group-name] :company_id company-id :id ['not= group-id]})
  (let [group-result (or (first (common-fn/select-record-count entities where-condition)) nil)]
(println group-result)
    ;; Check whether group already exists; if not update record
    (if (empty? group-result)
      (do
        (def set-fields-clause {:company_id company-id :group_name group-name :deleted deleted-status :status status})
        (def where-clause {:id group-id})

        ;;Update record
        (common-fn/update-record entities set-fields-clause where-clause)
        (ring-resp/response {:status 200}))
      (do
        (ring-resp/response {:status 409})))))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :group_name [[v/required :message "Group name is required"] [v/string :message "Group name can only have characters"]]))

(defn validate-group-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :group_name [[v/required :message "Group name is required"] [v/string :message "Group name can only have characters"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have characters"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have characters"]]))

(defn insert-group
  "Add new group for a particular user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "groups" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do
              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:group_name ['ilike (:group_name json-params)] :company_id (:company_id json-params)})
                    (let [group (or (first (common-fn/select-record authorization-entities/group where-clause)))]

                      ;; If group doesn't exists; create it
                      (if (empty? group)
                        (do

                          ;;Insert into "user_group" table
                          (common-fn/insert-record authorization-entities/group {:company_id (:company_id json-params) :group_name (:group_name json-params)})

                          ;;Send "Group created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{message:\"record already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn update-group
  "Update record of selected group"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "groups" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-group-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:group-id path-params))})
                    (let [usr (or (first (common-fn/select-record-count authorization-entities/group where-clause)))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? usr)
                        (do

                          ;;Update record
                          (let [update-resultset (update-record authorization-entities/group (common-fn/parse-number (:group-id path-params)) (:company_id json-params) (:group_name json-params) (:deleted_status json-params) (:status json-params))]
                            (if (= (get-in update-resultset [:body :status]) 409)
                              (ring-resp/response {:status 409 :body "{conflict:\"record already exists\"}"})
                              (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
              (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-group
  "Get the record of selected group"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "groups" group-id)]
          (if (= (get-in check-auth [:body :authorize]) true)
            (do
              (let [group-id (get-in request [:path-params :group-id])]
                group-id (if (empty? group-id) 0 group-id)
                (def where-clause {:id (common-fn/parse-number group-id)})
                (let [resultSet (or (first (common-fn/select-record authorization-entities/group where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
              (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))


(defn list-all-group
  "Get all the records from the 'user_group' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "groups" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                    row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                    search-value (if (empty? search-value) nil search-value)
                (def where-clause {:company_id company-id :group_name ['ilike (str "%" search-value "%")]})
                (let [resultSet (or (common-fn/select-record-with-list authorization-entities/group where-clause row-limit row-offset) nil)]

                ;; If user doesn't exists; insert record
                (if-not (empty? resultSet)
                  (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                  (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
              (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-group
  "Delete record of a selected group"
  [{:keys [headers params json-params path-params] :as request}]

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" "groups" group-id)]
          (if (= (get-in check-auth [:body :authorize]) true)
            (do
              (let [group-id (get-in request [:path-params :group-id])
                    group-id (if (empty? group-id) nil (common-fn/parse-number group-id))]
                (def where-clause {:id group-id})
                (let [usr-group (or (first (common-fn/select-record-count authorization-entities/group where-clause) ) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? usr-group)
                    (do
                      ;;delete from "user_group" table
                      (common-fn/logically-delete-record authorization-entities/group "yes" "inactive" :id group-id)

                      ;;delete from "group_permission" table
                      (common-fn/logically-delete-record authorization-entities/group_permission "yes" "inactive" :group_id group-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
              (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

