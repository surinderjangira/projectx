(ns ^{:doc "DB Entities for Authorizations"
      :author "Surinder Singh Jangira"}
    authorization.entities
    (:require [clojure.string :as str]
              [korma.core :as kc]
              [configuration.database :as db-config]
              [company.entities :as company-entities]))

(declare group group_permission)

(kc/defentity group
  (kc/pk :id)
  (kc/table :user_group)
  (kc/entity-fields :id :company_id :group_name :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to company-entities/company)
  (kc/has-many group_permission))

(kc/defentity group_permission
  (kc/pk :id)
  (kc/table :group_permission)
  (kc/entity-fields :id :group_id :company_id :permission :access_level :deleted :status :created_on)  ;; access level can be view, create, update
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to group))
