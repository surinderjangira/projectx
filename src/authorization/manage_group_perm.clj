(ns authorization.manage_group_perm
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [authorization.entities :as authorization-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]))

(defn update-record
  "Update vendor record in specified table"
  [entities permission-id group-id company-id permission-name access-level deleted-status status]
  (def where-condition {:permission ['ilike permission-name] :access_level ['ilike access-level] :company_id company-id :group_id group-id :id ['not= permission-id]})
  (let [group-result (or (first (common-fn/select-record-count entities where-condition)) nil)]

    ;; Check whether group already exists; if not update record
    (if (empty? group-result)
      (do
        ;;(def set-fields-clause {:company_id company-id :group_id group-id :permission permission-name :access_level access-level :deleted deleted-status :status status})
        ;;(def where-clause {:id permission-id})

        ;;Update record
        ;;(common-fn/update-record entities set-fields-clause where-clause)
        (ring-resp/response {:status 200}))
      (do
        (ring-resp/response {:status 409}))))
  )

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :group_id [[v/required :message "Group ID is required"] [v/integer :message "Group ID can only have integer value"]]
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :permission [[v/required :message "Permission is required"] [v/string :message "Permission can only have characters"]]
    :access_level [[v/required :message "Access level is required"] [v/string :message "Access level can only have characters"]]))

(defn validate-permission-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :group_id [[v/required :message "Group ID is required"] [v/integer :message "Group ID can only have integer value"]]
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :permission [[v/required :message "Permission is required"] [v/string :message "Permission can only have characters"]]
    :access_level [[v/required :message "Access level is required"] [v/string :message "Access level can only have characters"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have characters"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have characters"]]))

(defn insert-permission
  "Add new permission for a particular group"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "permissions" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:group_id (:group_id json-params) :access_level ['ilike (:access_level json-params)] :permission ['ilike (:permission json-params)]})
                    (let [group (or (first (common-fn/select-record authorization-entities/group_permission where-clause)))]

                      ;; If group doesn't exists; create it
                      (if (empty? group)
                        (do
                          ;;Insert into "user_group" table
                          (common-fn/insert-record authorization-entities/group_permission {:group_id (:group_id json-params) :company_id (:company_id json-params) :permission (:permission json-params) :access_level (:access_level json-params)})

                          ;;Send "Group created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{message:\"record already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn update-group-perm
  "Update record of selected group permission"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "permissions" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do
              ;;Validate data
              (let [errors (validate-permission-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:permission-id path-params))})
                    (let [permission-resultset (or (first (common-fn/select-record-count authorization-entities/group_permission where-clause)))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? permission-resultset)
                        (do

                          ;;Update record
                          ;;(let [update-resultset (update-record authorization-entities/group_permission (common-fn/parse-number (:permission-id path-params)) (common-fn/parse-number (:group_id json-params)) (common-fn/parse-number (:company_id json-params)) (:permission json-params) (:access_level json-params) (:deleted_status json-params) (:status json-params))]
                          (let [update-resultset (update-record authorization-entities/group_permission (:permission-id path-params) (:group_id json-params) (:company_id json-params) (:permission json-params) (:access_level json-params) (:deleted_status json-params) (:status json-params))]
                            (if (= (get-in update-resultset [:body :status]) 409)
                              (ring-resp/response {:status 409 :body "{conflict:\"record already exists\"}"})
                              (ring-resp/response {:status 200 :body "{updated:\"ok\"}"})))

                        )
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
              (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-group-perm
  "Get the record of selected group permission"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "permissions" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do
              (let [perm-id (get-in request [:path-params :permission-id])]
                perm-id (if (empty? perm-id) 0 perm-id)
                (def where-clause {:id (common-fn/parse-number perm-id)})
                (let [resultSet (or (first (common-fn/select-record authorization-entities/group_permission where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
              (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-group-perm
  "Get all the records from the 'group_permission' table"
  [{:keys [headers params json-params path-params] :as request}]

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "permissions" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                    row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                    search-value (if (empty? search-value) nil search-value)
                (def where-clause {:company_id company-id :permission ['ilike (str "%" search-value "%")]})
                (let [resultSet (or (common-fn/select-record-with-list authorization-entities/group_permission where-clause row-limit row-offset) nil)]

                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
              (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-group-perm
  "Delete record of a selected group permission"
  [{:keys [headers params json-params path-params] :as request}]

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" "permissions" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [perm-id (get-in request [:path-params :permission-id])
                    perm-id (if (empty? perm-id) nil (common-fn/parse-number perm-id))]
                (def where-clause {:id perm-id})
                (let [group-perm (or (first (common-fn/select-record-count authorization-entities/group_permission where-clause)) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? group-perm)
                    (do
                      ;;delete from "group_permission" table
                      (common-fn/logically-delete-record authorization-entities/group_permission "yes" "inactive" :id perm-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
              (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

