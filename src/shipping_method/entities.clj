(ns ^{:doc "DB Entities for a Shipping method"
      :author "Surinder Singh Jangira"}
    shipping_method.entities
  (:require [clojure.string :as str]
            [korma.core :as kc]
            [configuration.database :as db-config]
            ))

(declare shipping_method)

(kc/defentity shipping_method
  (kc/pk :id)
  (kc/table :shipping_method)
  (kc/entity-fields :id :channel_id :method_name :status)
  (kc/database db-config/projectxdb))
