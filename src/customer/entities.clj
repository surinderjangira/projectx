(ns ^{:doc "DB Entities for a Customer"
      :author "Surinder Singh Jangira"}
    customer.entities
  (:require [clojure.string :as str]
            [korma.core :as kc]
            [configuration.database :as db-config]))

(declare customer customer_address state country)

(kc/defentity customer
  (kc/pk :id)
  (kc/table :customer)
  (kc/entity-fields :id :channel_id :first_name :last_name :email :phone :shipping_address :billing_address :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/has-many customer_address)
  (kc/has-many line_item)
  (kc/has-many order)
  (kc/belongs-to channel))

(kc/defentity customer_address
  (kc/pk :id)
  (kc/table :customer_address)
  (kc/entity-fields :id :customer_id :street_address :city :state_id :country_id, :zip)
  (kc/database db-config/projectxdb)
  ;; mutations
  (kc/prepare (fn [{street_address :street_address :as v}]
             (if street_address
               (assoc v :street_address (str/lower-case street_address)) v)))
  ;; relationships
  (kc/belongs-to state)
  (kc/belongs-to country)
  (kc/belongs-to customer))

(kc/defentity state
  (kc/pk :id)
  (kc/table :state)
  (kc/entity-fields :id :country_id :code :name)
  (kc/database db-config/projectxdb)
  ;; mutations
  (kc/prepare (fn [{name :name :as v}]
             (if name
               (assoc v :name (str/lower-case name)) v)))
  ;; relationships
  (kc/belongs-to country)
  (kc/has-one customer_address))

(kc/defentity country
  (kc/pk :id)
  (kc/table :country)
  (kc/entity-fields :id :code2 :code3 :name)
  (kc/database db-config/projectxdb)
  ;; mutations
  (kc/prepare (fn [{name :name :as v}]
             (if name
               (assoc v :name (str/lower-case name)) v)))
  ;; relationships
  (kc/has-one customer_address)
  (kc/has-many state))
