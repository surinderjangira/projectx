(ns company.manage
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [company.entities :as company-entities]
            [authorization.entities :as authorization-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]
        [korma.db]))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :company_name [[v/required :message "Company name is required"] [v/string :message "Please enter characters for Company Name"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have characters"]]))

(defn validate-company-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_name [[v/required :message "Company name is required"] [v/string :message "Company name can only have alphabets"]]
    :address_id [[v/required :message "Address ID is required"] [v/integer :message "Address ID can only have integer"]]
    :logo_file_id [[v/required :message "Logo File ID is required"] [v/integer :message "logo File ID can only have integer"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-company
  "Add new company"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Validate data
  (let [errors (validate-data json-params)]

    ;;If "error" found; return error message
    (if-not (= (get errors 0) nil)
      (do
        (ring-resp/response {:status 400 :body (json/write-str errors)}))
      (do
        (def where-clause {:name (:company_name json-params)})
        (let [company (or (first (common-fn/select-record company-entities/company where-clause)))]

          ;; If user exists; send login success
          (if-not (empty? company)
            (do

              ;;Insert into "company" table
              (common-fn/insert-record company-entities/company {:name (:company_name json-params) :address_id (:address_id json-params) :logo_file_id (:logo_file_id json-params) :status "active"})

              ;;Send "User created" response
              (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
            (do
              ;;Send "User already exists" response
              (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))))

(defn update-company
  "Update record of selected company"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "company" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-company-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:company-id path-params))})
                    (let [usr (or (first (common-fn/select-record-count company-entities/company where-clause)))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? usr)
                        (do
                          (def set-fields-clause {:name (:company_name json-params) :address_id (:address_id json-params) :logo_file_id (:logo_file_id json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:company-id path-params))})

                          ;;Update record
                          (common-fn/update-record company-entities/company set-fields-clause where-clause)

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-company
  "Get the record of selected company"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "company" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [company-id (get-in request [:path-params :company-id])]
                company-id (if (empty? company-id) 0 company-id)
                (def where-clause {:id (common-fn/parse-number company-id)})

                (let [resultSet (or (first (common-fn/select-record company-entities/company where-clause)) nil)]
                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-companies
  "Get all the records from the 'company' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "company" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:path-params :offset])
                    row-limit (get-in request [:path-params :limit])
                    search-value (get-in request [:query-params :q])]
                row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                search-value (if (empty? search-value) nil search-value)

                (def where-clause {:id company-id :name ['ilike (str "%" search-value "%")]})
                (let [resultSet (or (common-fn/select-record-with-list company-entities/company where-clause row-limit row-offset) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-company
  "Delete record of a selected company"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "companys" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [company-id (get-in request [:path-params :company-id])
                    company-id (if (empty? company-id) nil (common-fn/parse-number company-id))]
                (def where-clause {:id company-id})
                (let [company (or (first (common-fn/select-record-count company-entities/company where-clause)) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? company)
                    (transaction
                      ;;delete from "users" table
                      (common-fn/logically-delete-record user-entities/user "yes" "inactive" :company_id company-id)

                      ;;delete from "company_channel_account" table
                      (common-fn/logically-delete-record company-entities/company_channel_account "yes" "inactive" :company_id company-id)

                      ;;delete from "email" table
                      (common-fn/logically-delete-record authorization-entities/group "yes" "inactive" :company_id company-id)

                      ;;delete from "phone" table
                      (common-fn/logically-delete-record authorization-entities/group_permission "yes" "inactive" :company_id company-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            )
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
