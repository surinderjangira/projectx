(ns ^{:doc "DB Entities for a Company"
      :author "Surinder Singh Jangira"}
    company.entities
    (:require [clojure.string :as str]
            [korma.core :as kc]
            [configuration.database :as db-config]
            [channel.entities :as channel-entities]))

(declare company company_channel_account)

(kc/defentity company
  (kc/pk :id)
  (kc/table :company)
  (kc/entity-fields :id :name :address_id :logo_file_id :deleted :status :created_on)
  (kc/database db-config/projectxdb))

(kc/defentity company_channel_account
  (kc/pk :id)
  (kc/table :company_channel_account)
  (kc/entity-fields :id :company_id :channel_id :channel_account_id :configuration :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to channel-entities/channel)
  (kc/belongs-to company))
