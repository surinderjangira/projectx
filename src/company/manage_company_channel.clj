(ns company.manage_company_channel
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [company.entities :as company-entities]
            [authorization.entities :as authorization-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :channel_id [[v/required :message "Channel ID is required"] [v/integer :message "Channel ID can only have integer value"]]
    :channel_account_id [[v/required :message "Channel Account ID is required"] [v/integer :message "Channel Account ID can only have integer value"]]
    :configuration [[v/required :message "Configuration is required"] [v/string :message "Configuration can only have characters"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have characters"]]))

(defn validate-channel-account-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :channel_id [[v/required :message "Channel ID is required"] [v/integer :message "Channel ID can only have integer value"]]
    :channel_account_id [[v/required :message "Channel Account ID is required"] [v/integer :message "Channel Account ID can only have integer value"]]
    :configuration [[v/required :message "Configuration is required"] [v/string :message "Configuration can only have characters"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have characters"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have characters"]]))

(defn insert-company-channel
  "Add new company channel account details"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "comp-channel" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:company_id (:company_id json-params) :channel_id (:channel_id json-params)})
                    (let [company (or (first (common-fn/select-record company-entities/company_channel_account where-clause)))]

                      ;; If user exists; send login success
                      (if (empty? company)
                        (do

                          ;;Insert into "company_channel_account" table
                          (common-fn/insert-record company-entities/company_channel_account {:company_id (:company_id json-params) :channel_id (:channel_id json-params) :channel_account_id (:channel_account_id json-params) :configuration (:configuration json-params) :status (:status json-params)})

                          ;;Send "User created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{message:\"record for this channel already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn update-company-channel
  "Update record of selected company channel account"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "comp-channel" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-channel-account-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:comp-channel-id path-params))})
                    (let [usr (or (first (common-fn/select-record-count company-entities/company_channel_account where-clause)))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? usr)
                        (do
                          (def set-fields-clause {:company_id (:company_id json-params) :channel_id (:channel_id json-params) :channel_account_id (:channel_account_id json-params) :configuration (:configuration json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:comp-channel-id path-params))})

                          ;;Update record
                          (common-fn/update-record company-entities/company_channel_account set-fields-clause where-clause)

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-company-channel
  "Get the record of selected company channel"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "comp-channel" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [comp-channel-id (get-in request [:path-params :comp-channel-id])]
                comp-channel-id (if (empty? comp-channel-id) 0 comp-channel-id)
                (def where-clause {:id (common-fn/parse-number comp-channel-id)})

                (let [resultSet (or (first (common-fn/select-record company-entities/company_channel_account where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-company-channel
  "Get all the records from the 'company_channel_acount' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "comp-channel" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:path-params :offset])
                    row-limit (get-in request [:path-params :limit])]
                row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))

                (def where-clause {:company_id company-id})
                (let [resultSet (or (common-fn/select-record-with-list company-entities/company_channel_account where-clause row-limit row-offset) nil)]

                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-company-channel
  "Delete record of a selected company channel account"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "companys" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [comp-channel-id (get-in request [:path-params :comp-channel-id])
                    comp-channel-id (if (empty? comp-channel-id) nil (common-fn/parse-number comp-channel-id))]
                (def where-clause {:id comp-channel-id})
                (let [company (or (first (common-fn/select-record-count company-entities/company_channel_account where-clause)) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? company)
                    (do
                      ;;delete from "company_channel_account" table
                      (common-fn/logically-delete-record company-entities/company_channel_account "yes" "inactive" :id comp-channel-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
