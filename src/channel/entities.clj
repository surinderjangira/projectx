(ns ^{:doc "DB Entities for a Channel"
      :author "Surinder Singh Jangira"}
    channel.entities
  (:require [clojure.string :as str]
            [korma.core :as kc]
            [configuration.database :as db-config]))

(declare channel)

(kc/defentity channel
  (kc/pk :id)
  (kc/table :channel)
  (kc/entity-fields :id :name :status)
  (kc/database db-config/projectxdb)
  ;; mutations
  (kc/prepare (fn [{name :name :as v}]
             (if name
               (assoc v :name (str/lower-case name)) v))))
