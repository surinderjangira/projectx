(ns user.change-pwd
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [contact.entities :as contact-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v])
  (:use [korma.core]))

(defn sha1-str
  "Encrypt the string"
  [s]
  (->> (-> "sha1"
           java.security.MessageDigest/getInstance
           (.digest (.getBytes s)))
       (map #(.substring
              (Integer/toString
               (+ (bit-and % 0xff) 0x100) 16) 1))
       (apply str)))

(defn select-record
  "Verify whether user already exists"
  [entities user-id old-password]
  (select entities
          (aggregate (count :users.id) :cnt :id)
          (where {:password old-password
                  :id user-id})
          (join 'email (= :email.record_id :id))
          (limit 1)))

(defn update-record
  "Update user record in specified table"
  [entities user-id new-password]
  (update entities
    (set-fields {:password new-password})
    (where {:id [= user-id]})))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :user_id [[v/required :message "User ID is required"] [v/integer :message "User ID can only have integer value"]]
    :old_password [[v/required :message "Old Password is required"] [v/min-count 8 :message "Old Password should be minimum 8 characters in length"]]
    :new_password [[v/required :message "New Password is required"] [v/min-count 8 :message "New Password should be minimum 8 characters in length"]]))

(defn change-password
  "Update password of selected user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Validate data
  (let [errors (validate-data json-params)]

    ;;If "error" found; return error message
    (if-not (= (get errors 0) nil)
      (do
        (ring-resp/response {:status 400 :body (json/write-str errors)}))
      (do

        (let [usr (or (first (select-record user-entities/user (:user_id json-params) (sha1-str (:old_password json-params)))))]

          ;; If user doesn't exists; insert record
          (if-not (empty? usr)
            (do
              (update-record user-entities/user (:user_id json-params) (sha1-str (:new_password json-params)))

              ;;Send "User updated" response
              (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
            (do
              ;;Send "User already exists" response
              (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
