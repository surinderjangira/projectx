(ns user.manage
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [contact.entities :as contact-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]
        [korma.db]))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :email [[v/required :message "Email is required"] [v/email :message "Enter a valid email address"]]
    :password [[v/required :message "Password is required"] [v/min-count 8 :message "Password should be minimum 8 characters in length"]]
    :first_name [[v/required :message "First name is required"] [v/string :message "First name can only have alphabets"]]
    :last_name [[v/required :message "Last name is required"] [v/string :message "Last name can only have alphabets"]]
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :group_id [[v/required :message "Group ID is required"] [v/integer :message "Group ID can only have integer value"]]
    :record_type [[v/required :message "Record Type is required"] [v/string :message "Record Type can only have alphabets"]]
    :access_type [[v/required :message "Access Type is required"] [v/string :message "Access Type can only have alphabets"]]
    :address_1 [[v/required :message "Address is required"] [v/string :message "Address can only have characters"]]
    :phone [[v/required :message "Phone is required"] [v/number :message "Phone can only have numbers"]]
    :city [[v/required :message "City name is required"] [v/string :message "City name can only have alphabets"]]
    :state_id [[v/required :message "Please enter numeric value for State ID"] [v/integer :message "State ID can only have integer value"]]
    :country_id [[v/required :message "Please enter numeric value for Country ID"] [v/integer :message "Country ID can only have integer value"]]
    :zip [[v/required :message "Zip is required"] [v/string :message "Zip is required"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn validate-user-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :group_id [[v/required :message "Group ID is required"] [v/integer :message "Group ID can only have integer value"]]
    :first_name [[v/required :message "First name is required"] [v/string :message "First name can only have alphabets"]]
    :last_name [[v/required :message "Last name is required"] [v/string :message "Last name can only have alphabets"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-user
  "Create a new user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string
  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" (:access_type json-params) group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:email.email (:email json-params) :users.company_id (:company_id json-params)})
                    (let [usr (or (first (common-fn/select-user-record-count user-entities/user where-clause)))]

                      ;; If user exists; send login success
                      (if (empty? usr)
                        (transaction

                          ;;Insert into "users" table
                          (let [user-insert-id (common-fn/insert-record user-entities/user {:company_id (:company_id json-params) :group_id (:group_id json-params) :password (common-fn/sha1-str (:password json-params)) :first_name (:first_name json-params) :last_name (:last_name json-params) :status (:status json-params)})]
                            (def user-id (:id user-insert-id))

                            ;;Insert into "email" table
                            (common-fn/insert-record contact-entities/email {:record_type (:record_type json-params) :record_id user-id :email (:email json-params) :status (:status json-params)})

                            ;;Insert into "address" table
                            (common-fn/insert-record contact-entities/address {:record_type (:record_type json-params) :record_id user-id :address_1 (:address_1 json-params) :address_2 (:address_2 json-params) :city (:city json-params) :state_id (:state_id json-params) :country_id (:country_id json-params) :zip (:zip json-params) :status (:status json-params)})

                            ;;Insert into "phone" table
                            (common-fn/insert-record contact-entities/phone {:record_type (:record_type json-params) :record_id user-id :phone (:phone json-params) :status (:status json-params)})

                            (if (= (:record_type json-params) "company")
                              (def type-id (:company_id json-params))
                              (def type-id (:vendor_id json-params)))

                            ;;insert into "access" table
                            (common-fn/insert-record user-entities/access {:user_id user-id :access_type (:record_type json-params) :type_id type-id :group_id (:group_id json-params) :status (:status json-params)})

                            ;;Send "User created" response
                            (ring-resp/response {:status 201 :body "{created:\"ok\"}"})))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn update-user
  "Update record of selected user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string
  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" (:access_type json-params) group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-user-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str (get errors 0))}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:user-id path-params))})
                    (let [usr (or (first (common-fn/select-record-count user-entities/user where-clause)))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? usr)
                        (do

                          ;;Update "users" record
                          (def set-fields-clause {:company_id (:company_id json-params) :group_id (:group_id json-params) :first_name (:first_name json-params) :last_name (:last_name json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:user-id path-params))})
                          (common-fn/update-record user-entities/user set-fields-clause where-clause)

                          ;;Update "access" record
                          (def set-fields-clause2 {:group_id (:group_id json-params)})
                          (def where-clause2 {:user_id (common-fn/parse-number (:user-id path-params))})
                          (common-fn/update-record user-entities/access set-fields-clause2 where-clause2 )

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-user
  "Get the record of selected user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]

    (if (> group-id 0)
      (do

        (if (str/includes? (:path-info request) "vendor_users")
          (do
            (def access_type "vendors")
            (def record_type "vendor"))
          (do
            (def access_type "users")
            (def record_type "company")))

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" access_type group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [user-id (get-in request [:path-params :user-id])]
                user-id (if (empty? user-id) 0 user-id)

                (let [resultSet (or (first (select user-entities/user
                                           (fields :email.email)
                                           (where {:email.record_id (common-fn/parse-number user-id)
                                                   :users.company_id company-id
                                                   :email.record_type record_type})
                                           (join 'email (= :email.record_id :id) )
                                           (limit 1))) nil)]
                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-users
  "Get all the records from the 'Users' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do
        (if (str/includes? (:path-info request) "vendor_users")
          (do
            (def access_type "vendors")
            (def record_type "vendor"))
          (do
            (def access_type "users")
            (def record_type "company")))

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" access_type group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                search-value (if (empty? search-value) nil search-value)

                (let [resultSet (or (select user-entities/user
                                         (aggregate (count :id) :cnt :id)
                                         (where {:users.company_id company-id
                                                 :email.record_type record_type})
                                         (where (or {:users.first_name [ilike (str "%" search-value "%")]} {:users.last_name [ilike (str "%" search-value "%")]} {:email.email [ilike (str "%" search-value "%")]}))
                                         (join 'email (= :email.record_id :id) )
                                         (order :id :DESC)
                                         (limit row-limit)
                                         (offset row-offset)) nil)]

                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))


(defn delete-user
  "Delete record of a selected user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do
        (if (str/includes? (:path-info request) "vendor_users")
          (do
            (def access_type "vendors")
            (def record_type "vendor"))
          (do
            (def access_type "users1")
            (def record_type "company")))

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" access_type group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [user-id (get-in request [:path-params :user-id])]
                user-id (if (empty? user-id) 0 user-id)

                (let [usr (or (first (select user-entities/user
                                     (aggregate (count :users.id) :cnt :id)
                                     (where {:users.id (common-fn/parse-number user-id)
                                             :users.company_id company-id
                                             :email.record_type record_type})
                                     (join 'email (= :email.record_id :id) )
                                     (limit 1))) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? usr)
                    (transaction
                      ;;delete from "users" table
                      (common-fn/logically-delete-record user-entities/user "yes" "inactive" :id user-id)

                      ;;delete from "address" table
                      (common-fn/logically-delete-record contact-entities/address "yes" "inactive" :record_id user-id)

                      ;;delete from "email" table
                      (common-fn/logically-delete-record contact-entities/email "yes" "inactive" :record_id user-id)

                      ;;delete from "phone" table
                      (common-fn/logically-delete-record contact-entities/phone "yes" "inactive" :record_id user-id)

                      ;;delete from "access" table
                      (common-fn/logically-delete-record user-entities/access "yes" "inactive" :user_id user-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
