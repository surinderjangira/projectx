(ns ^{:doc "DB Entities for an User"
      :author "Surinder Singh Jangira"}
    user.entities
    (:require [clojure.string :as str]
              [korma.core :as kc]
              [configuration.database :as db-config]
              [authorization.entities :as authorization-entities]
              [company.entities :as company-entities]
              [contact.entities :as contact-entities]))

(declare user)

(kc/defentity user
  (kc/pk :id)
  (kc/table :users)
  (kc/entity-fields :id :company_id :group_id :password :first_name :last_name :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/has-many contact-entities/address)
  (kc/has-many contact-entities/email)
  (kc/has-many contact-entities/phone)
  (kc/belongs-to company-entities/company)
  (kc/belongs-to authorization-entities/group))

(kc/defentity access
  (kc/pk :id)
  (kc/table :access)
  (kc/entity-fields :id :user_id :access_type :type_id :group_id :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to user))
