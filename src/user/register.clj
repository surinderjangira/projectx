(ns user.register
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [contact.entities :as contact-entities]
            [company.entities :as company-entities]
            [authorization.entities :as authorization-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v])
  (:use [korma.core]
        [korma.db]))

(defn sha1-str
  "Encrypt the string"
  [s]
  (->> (-> "sha1"
           java.security.MessageDigest/getInstance
           (.digest (.getBytes s)))
       (map #(.substring
              (Integer/toString
               (+ (bit-and % 0xff) 0x100) 16) 1))
       (apply str)))

(defn parse-number
  "Reads a number from a string. Returns nil if not a number."
  [s]
  (if (re-find #"^-?\d+\.?\d*$" s)
    (read-string s)))

(defn insert-record
  "Insert record in specified table"
  [entities field-values]
  (insert entities
    (values field-values)))

(defn select-record
  "Verify whether user already exists"
  [entities company-name]
  (select entities
          (aggregate (count :id) :cnt :id)
          (where {:name [ilike company-name]})
          (limit 1)))

(defn update-record
  "Update record in specified table"
  [entities field-status field-name field-value]
  (update entities
    (set-fields {:status field-status})
    (where {field-name [= field-value]})))

(defn activate-user
  "Activate an specified user account in the database"
  [user-id]
  (update-record user-entities/user "active" :id user-id)
  (update-record contact-entities/email "active" :record_id user-id)
  (update-record contact-entities/address "active" :record_id user-id)
  (update-record contact-entities/phone "active" :record_id user-id)
  (update-record user-entities/access "active" :user_id user-id))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
      :email [[v/required :message "Email is required"] [v/email :message "Enter a valid email address"]]
      :password [[v/required :message "Password is required"] [v/min-count 8 :message "Password should be minimum 8 characters in length"]]
      :first_name [[v/required :message "First name is required"] [v/string :message "First name can only have alphabets"]]
      :company_name [[v/required :message "Company name is required"] [v/string :message "Company name can only have alphabets"]]))

(defn register-me
  "Register the user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Validate data
  (let [errors (validate-data json-params)]

    ;;If "error" found; return error message
    (if-not (= (get errors 0) nil)
      (do
        (ring-resp/response {:status 400 :body (json/write-str errors)}))
      (do
        (let [company (or (first (select-record company-entities/company (:company_name json-params))))]

          ;; If user exists; send login success
          (if (empty? company)
            (transaction

               ;;Insert into "company" table
               (let [company-resultset (insert-record company-entities/company {:name (:company_name json-params) :status "active"})]
                 (def company-id (:id company-resultset))

                 ;;Insert into "users" table
                 (let [user-insert-id (insert-record user-entities/user {:company_id company-id :group_id 1 :password (sha1-str (:password json-params)) :first_name (:first_name json-params)})]
                   (def user-id (:id user-insert-id))

                 ;;Insert into "email" table
                 (insert-record contact-entities/email {:record_type "company" :record_id user-id :email (:email json-params)}))

                 (let [group-resultset (insert-record authorization-entities/group {:company_id company-id :group_name "suadmin" :status "active"})]
                   (def group-id (:id group-resultset))

                   ;;insert into "access" table
                   (insert-record user-entities/access {:user_id user-id :access_type "company" :type_id company-id :group_id group-id}))

                 ;;Mail the activation link

                 ;;Send "User created" response
                 (ring-resp/response {:status 201 :body "{created:\"ok\"}"})))
              (do
                ;;Send "User already exists" response
                (ring-resp/response {:status 409 :body "{conflict:\"record already exists\"}"}))))))))

(defn activate-me
  "Activate user's account in the database"
  [request]
  (let [company-id (get-in request [:params :company_id])
        company-id (if (empty? company-id) nil (parse-number company-id))
        email (get-in request [:params :email])
        email (if (empty? email) nil (str/trim email))]

    (let [usr (or (first (select user-entities/user
                             (fields :id :users.status)
                             (where {:email.email email
                                     :company_id company-id})
                             (join 'email (= :email.record_id :id))
                             (limit 1))) nil)]
      ;; If user doesn't exists; update record
      (if-not (empty? usr)
        (do
          (def user-id (:id usr))
          (def user-status (read-string (:status usr)))
          (if (= user-status 'active)
            (do
              (ring-resp/response {:status 409 :body "{conflict:\"user already active\"}"}))
            (do
              (activate-user user-id)
              (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))))
        (do
          ;;Send "User already exists" response
          (ring-resp/response {:status 400 :body "{updated:\"user does not exist\"}"}))))))
