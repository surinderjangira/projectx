(ns ^{:doc "DB Entities for an Order"
      :author "Surinder Singh Jangira"}
    order.entities
  (:require [clojure.string :as str]
            [korma.core :as kc]
            [configuration.database :as db-config]
            [channel.entities :as channel-entities]
            [shipping_method.entities :as shipping-method-entities]
            [customer.entities :as customer-entities]))

(declare order line_item)

(kc/defentity order
  (kc/pk :id)
  (kc/table :order)
  (kc/entity-fields :id :customer_id :shipping_method_id :payment_method_id :billing_address_id :shipping_address_id :order_datetime :order_number :sub_total :total :payment_processed :customer_notes :shipping_option :coupon_number :discount :free_shipping)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/has-many line_item)
  (kc/belongs-to customer)
  (kc/belongs-to line_item)
  (kc/belongs-to shipping-method-entities/shipping_method)))

(kc/defentity line_item
  (kc/pk :id)
  (kc/table :line_item)
  (kc/entity-fields :id :channel_id :customer_id :product_id :order_id :quantity :comments :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to customer-entities/customer)
  (kc/belongs-to order)
  (kc/belongs-to channel-entities/channel))
