(ns vendor.manage
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [company.entities :as company-entities]
            [contact.entities :as contact-entities]
            [authorization.entities :as authorization-entities]
            [vendor.entities :as vendor-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]
        [korma.db]))

(defn update-record
  "Update vendor record in specified table"
  [entities vendor-id company-id vendor-name deleted-status status]
  (def where-condition {:name ['ilike vendor-name] :company_id company-id :id ['not= vendor-id]})
  (let [vendor-result (or (first (common-fn/select-record-count vendor-entities/vendor where-condition)) nil)]

    ;; Check whether vendor already exists; if not update record
    (if (empty? vendor-result)
      (do
        (def set-fields-clause {:company_id company-id :name vendor-name :deleted deleted-status :status status})
        (def where-clause {:id vendor-id})

        ;;Update record
        (common-fn/update-record entities set-fields-clause where-clause)
        (ring-resp/response {:status 200}))
      (do
        (ring-resp/response {:status 409})))))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :vendor_name [[v/required :message "Vendor name is required"] [v/string :message "Please enter characters for Vendor Name"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have characters"]]))

(defn validate-vendor-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :vendor_name [[v/required :message "Vendor name is required"] [v/string :message "Vendor name can only have alphabets"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-vendor
  "Create a new vendor"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]

    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:name (:vendor_name json-params) :company_id company-id})
                    (let [usr (or (first (common-fn/select-record vendor-entities/vendor where-clause)))]

                      ;; If user exists; send login success
                      (if (empty? usr)
                        (do

                          ;;Insert into "email" table
                          (common-fn/insert-record vendor-entities/vendor {:company_id company-id :name (:vendor_name json-params) :status (:status json-params)})

                          ;;Send "User created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{message:\"Vendor already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn update-vendor
  "Update record of selected vendor"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-vendor-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:vendor-id path-params))})
                    (let [usr (or (first (common-fn/select-record-count vendor-entities/vendor where-clause)))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? usr)
                        (do
                          (let [update-resultset (update-record vendor-entities/vendor (common-fn/parse-number (:vendor-id path-params)) (:company_id json-params) (:vendor_name json-params) (:deleted_status json-params) (:status json-params))]
                            (if (= (get-in update-resultset [:body :status]) 409)
                              (ring-resp/response {:status 409 :body "{conflict:\"record already exists\"}"})
                              (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))))
                        (do
                          ;;Send "Vendor already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-vendor
  "Get the record of selected address"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [vendor-id (get-in request [:path-params :vendor-id])]
                vendor-id (if (empty? vendor-id) 0 vendor-id)
                (def where-clause {:id (common-fn/parse-number vendor-id)})
                (let [resultSet (or (first (common-fn/select-record vendor-entities/vendor where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-vendor
  "Get all the records from the 'address' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                    row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                    search-value (if (empty? search-value) nil search-value)
                (def where-clause {:company_id company-id :name ['ilike (str "%" search-value "%")]})

                (let [resultSet (or (common-fn/select-record-with-list vendor-entities/vendor where-clause row-limit row-offset) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-vendor
  "Delete record of a selected vendor"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do
                (let [vendor-id (get-in request [:path-params :vendor-id])
                    vendor-id (if (empty? vendor-id) nil (common-fn/parse-number vendor-id))]
                  (def where-clause {:id vendor-id})
                  (let [vendor-result (or (common-fn/select-record vendor-entities/vendor where-clause) nil)]

                    ;; If user exists; send login success
                    (if-not (empty? vendor-result)
                      (do
                        (def where-clause2 {:type_id vendor-id :access_type "vendor" :status "active"})
                        (let [vendors (or (common-fn/select-all-records user-entities/access where-clause2) nil)]
                          (if-not (empty? vendors)
                            (do

                              ;;Iterate through user-ids and delete records
                              (doseq [rec vendors]
                                (let [user-id (:user_id rec)]

                                  ;;delete from "users" table
                                  (common-fn/logically-delete-record user-entities/user "yes" "inactive" :id user-id)

                                  ;;delete from "company_channel_account" table
                                  (common-fn/logically-delete-record contact-entities/address "yes" "inactive" :record_id user-id)

                                  ;;delete from "email" table
                                  (common-fn/logically-delete-record contact-entities/email "yes" "inactive" :record_id user-id)

                                  ;;delete from "phone" table
                                  (common-fn/logically-delete-record contact-entities/phone "yes" "inactive" :record_id user-id)))

                              ;;Delete from "access" table
                              (common-fn/logically-delete-record user-entities/access "yes" "inactive" :type_id vendor-id)

                              ;;Send "Vendor deleted" response
                              (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                            (do

                              ;;Send "Vendor not found" response
                              (ring-resp/response {:status 204 :body "{message:\"no content / already deleted\"}"})))))
                      (do
                        ;;Send "Vendor not found" response
                        (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

