(ns vendor.manage_vendor_user
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [company.entities :as company-entities]
            [authorization.entities :as authorization-entities]
            [vendor.entities :as vendor-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [user.manage :as user-mgmt])
  (:use [korma.core]
        [korma.db])
  (:import java.util.Base64))

(defn decode [to-decode]
  (String. (.decode (Base64/getDecoder) to-decode)))

(defn sha1-str
  "Encrypt the string"
  [s]
  (->> (-> "sha1"
           java.security.MessageDigest/getInstance
           (.digest (.getBytes s)))
       (map #(.substring
              (Integer/toString
               (+ (bit-and % 0xff) 0x100) 16) 1))
       (apply str)))

(defn parse-number
  "Reads a number from a string. Returns nil if not a number."
  [s]
  (if (re-find #"^-?\d+\.?\d*$" s)
    (read-string s)))

(defn insert-record
  "Insert record in specified table"
  [entities field-values]
  (insert entities
    (values field-values)))

(defn select-record
  "Verify whether company already exists"
  [entities vendor-name company-id]
  (select entities
          (where {:name vendor-name
                  :company_id company-id})
          (limit 1)))

(defn select-vendor-count
  "Verify whether vendor already exists"
  [entities vendor-id]
  (select entities
          (aggregate (count :id) :cnt :id)
          (where {:id vendor-id})
          (limit 1)))

(defn update-record
  "Update vendor record in specified table"
  [entities vendor-id company-id vendor-name deleted-status status]
  (let [vendor-result (or (first (select vendor-entities/vendor
                               (aggregate (count :id) :cnt :id)
                               (where {:name [ilike vendor-name]
                                       :company_id company-id
                                       :id [not= vendor-id]})
                               (limit 1))) nil)]
      ;; If user exists; send login success
      (if (empty? vendor-result)
        (do
          (update entities
            (set-fields {:name vendor-name
                         :company_id company-id
                         :deleted deleted-status
                         :status status})
            (where {:id [= vendor-id]}))
            (ring-resp/response {:status 200}))
        (do
          (ring-resp/response {:status 409})
          ))))

(defn logically-delete-record
  "Update record in specified table"
  [entities deleted-field-status field-status field-name field-value]
  (update entities
    (set-fields {:deleted deleted-field-status
                 :status field-status})
    (where {field-name [= field-value]})))

(defn my-value-writer
  [key value]
  (if (= key :created_on)
    (str (java.sql.Date. (.getTime value)))
    value))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :vendor_name [[v/required :message "Vendor name is required"] [v/string :message "Please enter characters for Vendor Name"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have characters"]]))

(defn validate-vendor-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :vendor_name [[v/required :message "Vendor name is required"] [v/string :message "Vendor name can only have alphabets"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-vendor-user
  "Create a new vendor user"
  [{:keys [headers params json-params path-params] :as request}]
  (user-mgmt/insert-user {:keys [headers params json-params path-params] :as request})
  )

(defn insert-vendor-user-n
  "Create a new vendor"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (println company-id)

    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do

                    (let [usr (or (first (select-record vendor-entities/vendor (:vendor_name json-params) company-id)))]

                      ;; If user exists; send login success
                      (if (empty? usr)
                        (do

                          ;;Insert into "email" table
                          (insert-record vendor-entities/vendor {:company_id company-id :name (:vendor_name json-params) :status (:status json-params)})

                          ;;Send "User created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn update-vendor
  "Update record of selected vendor"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-vendor-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (let [usr (or (first (select-vendor-count vendor-entities/vendor (parse-number (:vendor-id path-params)))))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? usr)
                        (do
                          (let [update-resultset (update-record vendor-entities/vendor (parse-number (:vendor-id path-params)) (:company_id json-params) (:vendor_name json-params) (:deleted_status json-params) (:status json-params))]
                            (if (= (get-in update-resultset [:body :status]) 409)
                              (ring-resp/response {:status 409 :body "{conflict:\"record already exists\"}"})
                              (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-vendor
  "Get the record of selected address"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [vendor-id (get-in request [:path-params :vendor-id])]
                vendor-id (if (empty? vendor-id) 0 vendor-id)

                (let [resultSet (or (first (select vendor-entities/vendor
                                           (where {:id (parse-number vendor-id)})
                                           (limit 1))) nil)]
                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-vendor
  "Get all the records from the 'address' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])]
                    row-offset (if (empty? row-offset) 0 (parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (parse-number row-limit))

                (let [resultSet (or (select vendor-entities/vendor
                                         (aggregate (count :id) :cnt :id)
                                         (where {:company_id company-id})
                                         (order :id :DESC)
                                         (limit row-limit)
                                         (offset row-offset)) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-vendor
  "Delete record of a selected vendor"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" "vendors" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [company-id (get-in request [:path-params :company-id])
                    company-id (if (empty? company-id) nil (parse-number company-id))]

                (let [company (or (first (select vendor-entities/vendor
                                     (aggregate (count :id) :cnt :id)
                                     (where {:company_id company-id})
                                     (limit 1))) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? company)
                    (transaction
                      ;;delete from "users" table
                      ;;(logically-delete-record user-entities/user "yes" "inactive" :company_id company-id)

                      ;;delete from "company_channel_account" table
                      ;;(logically-delete-record company-entities/company_channel_account "yes" "inactive" :company_id company-id)

                      ;;delete from "email" table
                      ;;(logically-delete-record authorization-entities/group "yes" "inactive" :company_id company-id)

                      ;;delete from "phone" table
                      ;;(logically-delete-record authorization-entities/group_permission "yes" "inactive" :company_id company-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            )
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

