(ns ^{:doc "DB Entities for a Company"
      :author "Surinder Singh Jangira"}
    vendor.entities
    (:require [clojure.string :as str]
            [korma.core :as kc]
            [configuration.database :as db-config]
            [channel.entities :as channel-entities]))

(declare vendor)

(kc/defentity vendor
  (kc/pk :id)
  (kc/table :vendor)
  (kc/entity-fields :id :name :deleted :status :created_on)
  (kc/database db-config/projectxdb))
