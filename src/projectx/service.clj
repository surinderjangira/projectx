(ns projectx.service
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [ring.middleware.session.cookie :as cookie]
            [login.authenticate :as authenticate]
            [user.register :as registration]
            [user.manage :as user-mgmt]
            [user.change-pwd :as pwd-mgmt]
            [contact.manage_address :as address-mgmt]
            [contact.manage_phone :as phone-mgmt]
            [contact.manage_email :as email-mgmt]
            [company.manage :as company-mgmt]
            [company.manage_company_channel :as company-channel-mgmt]
            [authorization.manage_group :as group-mgmt]
            [authorization.manage_group_perm :as group-perm-mgmt]
            [vendor.manage :as vendor-mgmt]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [authorization.group_permissions :as auth-group-perm]
            [product.manage_category :as category-mgmt]
            [product.manage_category_attributes :as category-attribute-mgmt]
            [product.manage_product :as product-mgmt]
            [product.manage_product_attributes :as product-attribute-mgmt]))

(defn about-page
  [request]
  (ring-resp/response (format "Clojure %s - served from %s"
                              (clojure-version)
                              (route/url-for ::about-page))))

(defn home-page
  [request]
  (ring-resp/response "Hello World!"))

;;handler
(defn check-authorization
  [{:keys [headers params json-params path-params] :as request} access-level permission]
  (let [group-id (get (json/read-str (get-in (authenticate/authenticate-user-token headers) [:body])) "group-id" )]
      (if (> group-id 0)
        (do

          ;;Check whether the logged-in user has permissions to access the funtionality
          (let [check-auth (auth-group-perm/check-authorization access-level permission group-id)]

            (if (= (get-in check-auth [:body :authorize]) true)
              (do
                (ring-resp/response {:status 200 :body "{message:\"Authorized access\"}"}))
              (do
                (ring-resp/response {:status 401 :body "{message:\"Unauthorized access\"}"})))))
        (do
          (ring-resp/response {:status 401 :body "{message:\"Unauthorized access\"}"})))))

;;middleware function
;;(defn wrap-check-authorization
;;  [check-authorization-fn]
;;  (fn [request]
;;    (check-authorization-fn request)))

;;apply middleware to handler
;;(def app
;;  (wrap-check-authorization check-authorization {:keys [headers params json-params path-params] :as request} access-level permission))

;; Defines "/" and "/about" routes with their associated :get handlers.
;; The interceptors defined after the verb map (e.g., {:get home-page}
;; apply to / and its children (/about).
(def common-interceptors [(body-params/body-params) http/html-body middlewares/params middlewares/keyword-params])

;; Tabular routes
(def routes #{["/" :get (conj common-interceptors `home-page)]
              ["/about" :get (conj common-interceptors `about-page)]
              ["/authenticate" :post (conj common-interceptors `authenticate/authenticate-me)]
              ["/register" :post (conj common-interceptors `registration/register-me)]
              ["/register" :put (conj common-interceptors `registration/activate-me)]
              ["/users" :get (conj common-interceptors `user-mgmt/list-all-users) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all users
              ["/users" :post (conj common-interceptors `user-mgmt/insert-user)] ;; insert user
              ["/users/:user-id" :get (conj common-interceptors `user-mgmt/get-selected-user) :constraints {:user-id #"[0-9]+"}] ;; get a specific user record
              ["/users/:user-id" :patch (conj common-interceptors `user-mgmt/update-user) :constraints {:user-id #"[0-9]+"}] ;; update specified user record
              ["/users/:user-id" :delete (conj common-interceptors `user-mgmt/delete-user) :constraints {:user-id #"[0-9]+"}] ;; delete specific user record
              ["/addresses" :get (conj common-interceptors `address-mgmt/list-all-address) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all addresses
              ["/addresses" :post (conj common-interceptors `address-mgmt/insert-address)] ;; insert address
              ["/addresses/:address-id" :get (conj common-interceptors `address-mgmt/get-selected-address) :constraints {:address-id #"[0-9]+"}] ;; get a specific address record
              ["/addresses/:address-id" :patch (conj common-interceptors `address-mgmt/update-address) :constraints {:address-id #"[0-9]+"}] ;; update specified address record
              ["/addresses/:address-id" :delete (conj common-interceptors `address-mgmt/delete-address) :constraints {:address-id #"[0-9]+"}] ;; delete specific address record
              ["/phones" :get (conj common-interceptors `phone-mgmt/list-all-phones) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all phone numbers
              ["/phones" :post (conj common-interceptors `phone-mgmt/insert-phone)] ;; insert phone number for a specific user
              ["/phones/:phone-id" :get (conj common-interceptors `phone-mgmt/get-selected-phone) :constraints {:phone-id #"[0-9]+"}] ;; get a specific phone record of a specific user
              ["/phones/:phone-id" :patch (conj common-interceptors `phone-mgmt/update-phone) :constraints {:phone-id #"[0-9]+"}] ;; update specified phone record
              ["/phones/:phone-id" :delete (conj common-interceptors `phone-mgmt/delete-phone) :constraints {:phone-id #"[0-9]+"}] ;; delete specific phone record
              ["/emails/:email-id" :patch (conj common-interceptors `email-mgmt/update-email) :constraints {:email-id #"[0-9]+"}] ;; update specified email record
              ["/emails/:email-id" :get (conj common-interceptors `email-mgmt/get-selected-email) :constraints {:email-id #"[0-9]+"}] ;; get a specific email record
              ["/emails" :get (conj common-interceptors `email-mgmt/list-all-emails) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all emails
              ["/companies" :get (conj common-interceptors `company-mgmt/list-all-companies) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all companies
              ["/companies/:company-id" :get (conj common-interceptors `company-mgmt/get-selected-company) :constraints {:company-id #"[0-9]+"}] ;; get a specific phone record of a specific user
              ["/companies/:company-id" :patch (conj common-interceptors `company-mgmt/update-company) :constraints {:company-id #"[0-9]+"}] ;; update specified company record
              ["/companies" :post (conj common-interceptors `company-mgmt/insert-company)] ;; insert company
              ["/companies/:company-id" :delete (conj common-interceptors `company-mgmt/delete-company) :constraints {:company-id #"[0-9]+"}] ;; delete specific company record
              ["/company_channels" :post (conj common-interceptors `company-channel-mgmt/insert-company-channel)] ;; insert company channel account details
              ["/company_channels" :get (conj common-interceptors `company-channel-mgmt/list-all-company-channel) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all company channel account
              ["/company_channels/:comp-channel-id" :get (conj common-interceptors `company-channel-mgmt/get-selected-company-channel) :constraints {:comp-channel-id #"[0-9]+"}] ;; get a specific company channel record of a specific user
              ["/company_channels/:comp-channel-id" :patch (conj common-interceptors `company-channel-mgmt/update-company-channel) :constraints {:comp-channel-id #"[0-9]+"}] ;; update specified company channel account record
              ["/company_channels/:comp-channel-id" :delete (conj common-interceptors `company-channel-mgmt/delete-company-channel) :constraints {:comp-channel-id #"[0-9]+"}] ;; delete specific company channel account record
              ["/groups" :post (conj common-interceptors `group-mgmt/insert-group)] ;; insert company
              ["/groups/:group-id" :patch (conj common-interceptors `group-mgmt/update-group) :constraints {:group-id #"[0-9]+"}] ;; update specific group record
              ["/groups/:group-id" :get (conj common-interceptors `group-mgmt/get-selected-group) :constraints {:group-id #"[0-9]+"}] ;; get a specific group record
              ["/groups" :get (conj common-interceptors `group-mgmt/list-all-group) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all groups
              ["/groups/:group-id" :delete (conj common-interceptors `group-mgmt/delete-group) :constraints {:group-id #"[0-9]+"}] ;; delete specific group record
              ["/permissions" :post (conj common-interceptors `group-perm-mgmt/insert-permission)] ;; insert group permission
              ["/permissions/:permission-id" :get (conj common-interceptors `group-perm-mgmt/get-selected-group-perm) :constraints {:permission-id #"[0-9]+"}] ;; get a specific group permission record
              ["/permissions" :get (conj common-interceptors `group-perm-mgmt/list-all-group-perm) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all group permissions
              ["/permissions/:permission-id" :patch (conj common-interceptors `group-perm-mgmt/update-group-perm) :constraints {:permission-id #"[0-9]+"}] ;; update specific group permission record
              ["/permissions/:permission-id" :delete (conj common-interceptors `group-perm-mgmt/delete-group-perm) :constraints {:permission-id #"[0-9]+"}] ;; delete specific group permission record
              ["/change_pwd" :post (conj common-interceptors `pwd-mgmt/change-password)]
              ["/vendors" :post (conj common-interceptors `vendor-mgmt/insert-vendor)] ;; insert vendor
              ["/vendors/:vendor-id" :patch (conj common-interceptors `vendor-mgmt/update-vendor) :constraints {:vendor-id #"[0-9]+"}] ;; update specified company record
              ["/vendors/:vendor-id" :get (conj common-interceptors `vendor-mgmt/get-selected-vendor) :constraints {:vendor-id #"[0-9]+"}] ;; get a specific phone record of a specific user
              ["/vendors" :get (conj common-interceptors `vendor-mgmt/list-all-vendor) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all companies
              ["/vendors/:vendor-id" :delete (conj common-interceptors `vendor-mgmt/delete-vendor) :constraints {:vendor-id #"[0-9]+"}] ;; delete specific company record})
              ["/vendor_users" :post (conj common-interceptors `user-mgmt/insert-user) :route-name :add-vendor-user] ;; insert vendor user
              ["/vendor_users/:user-id" :patch (conj common-interceptors `user-mgmt/update-user) :route-name :update-vendor-user :constraints {:user-id #"[0-9]+"}] ;; update specified user record
              ["/vendor_users/:user-id" :get (conj common-interceptors `user-mgmt/get-selected-user) :route-name :get-vendor-user :constraints {:user-id #"[0-9]+"}] ;; get a specific user record
              ["/vendor_users" :get (conj common-interceptors `user-mgmt/list-all-users) :route-name :get-all-vendor-user :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all users
              ["/vendor_users/:user-id" :delete (conj common-interceptors `user-mgmt/delete-user) :route-name :delete-vendor-user :constraints {:user-id #"[0-9]+"}] ;; delete specific user record

              ["/categories" :post (conj common-interceptors `category-mgmt/insert-category)] ;; insert category
              ["/categories/:category-id" :patch (conj common-interceptors `category-mgmt/update-category) :constraints {:category-id #"[0-9]+"}] ;; update specified category record
              ["/categories/:category-id" :get (conj common-interceptors `category-mgmt/get-selected-category) :constraints {:category-id #"[0-9]+"}] ;; get a specific category record
              ["/categories" :get (conj common-interceptors `category-mgmt/list-all-categories) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all categories
              ["/categories/:category-id" :delete (conj common-interceptors `category-mgmt/delete-category) :constraints {:category-id #"[0-9]+"}] ;; delete specific company record})

              ["/category_attributes" :post (conj common-interceptors `category-attribute-mgmt/insert-category-attribute)] ;; insert category attribute
              ["/category_attributes/:cat-attribute-id" :patch (conj common-interceptors `category-attribute-mgmt/update-category-attribute) :constraints {:cat-attribute-id #"[0-9]+"}] ;; update specified category attribute record
              ["/category_attributes/:cat-attribute-id" :get (conj common-interceptors `category-attribute-mgmt/get-selected-category-attribute) :constraints {:cat-attribute-id #"[0-9]+"}] ;; get a specific category attribute record
              ["/category_attributes" :get (conj common-interceptors `category-attribute-mgmt/list-all-category-attributes) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all category attributes
              ["/category_attributes/:cat-attribute-id" :delete (conj common-interceptors `category-attribute-mgmt/delete-category-attribute) :constraints {:cat-attribute-id #"[0-9]+"}] ;; delete specific address record

              ["/products" :post (conj common-interceptors `product-mgmt/insert-product)] ;; insert products
              ["/products/:product-id" :patch (conj common-interceptors `product-mgmt/update-product) :constraints {:product-id #"[0-9]+"}] ;; update specified product record
              ["/products/:product-id" :get (conj common-interceptors `product-mgmt/get-selected-product) :constraints {:product-id #"[0-9]+"}] ;; get a specific product record
              ["/products" :get (conj common-interceptors `product-mgmt/list-all-products) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all products

              ["/product-attributes" :post (conj common-interceptors `product-attribute-mgmt/insert-product-attribute)] ;; insert product attributes
              ["/product-attributes/:prod-attribute-id" :patch (conj common-interceptors `product-attribute-mgmt/update-product-attribute) :constraints {:prod-attribute-id #"[0-9]+"}] ;; update specified product attribute record
              ["/product-attributes/:prod-attribute-id" :get (conj common-interceptors `product-attribute-mgmt/get-selected-product-attribute) :constraints {:prod-attribute-id #"[0-9]+"}] ;; get a specific product attribute record
              ["/product-attributes" :get (conj common-interceptors `product-attribute-mgmt/list-all-products-attribute) :constraints {:offset #"[0-9]+" :limit #"[0-9]+"}] ;; list all category attributes
              ["/product-attributes/:prod-attribute-id" :delete (conj common-interceptors `product-attribute-mgmt/delete-product-attribute) :constraints {:prod-attribute-id #"[0-9]+"}] ;; delete specific prduct attribute record
              })

;; Consumed by projectx.server/create-server
;; See http/default-interceptors for additional options you can configure
(def service {:env :prod
              ;; You can bring your own non-default interceptors. Make
              ;; sure you include routing and set it up right for
              ;; dev-mode. If you do, many other keys for configuring
              ;; default interceptors will be ignored.
              ;; ::http/interceptors []
              ::http/routes routes

              ;; Uncomment next line to enable CORS support, add
              ;; string(s) specifying scheme, host and port for
              ;; allowed source(s):
              ;;
              ;; "http://localhost:8080"
              ;;
              ;;::http/allowed-origins ["scheme://host:port"]

              ;; Root for resource interceptor that is available by default.
              ::http/resource-path "/public"

              ;; Either :jetty, :immutant or :tomcat (see comments in project.clj)
              ::http/type :jetty
              ;;::http/host "localhost"
              ::http/port 8080
              ;; Options to pass to the container (Jetty)
              ::http/container-options {:h2c? true
                                        :h2? false
                                        ;:keystore "test/hp/keystore.jks"
                                        ;:key-password "password"
                                        ;:ssl-port 8443
                                        :ssl? false}})

