(ns contact.manage_phone
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [contact.entities :as contact-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]))

(defn insert-record
  "Insert record in specified table"
  [entities field-values]
  (insert entities
    (values field-values)))

(defn select-record
  "Verify whether user already exists"
  [entities record-id]
  (select entities
          (where {:id record-id})
          (join 'email (= :email.record_id :id))
          (limit 1)))

(defn select-phone-count
  "Verify whether user already exists"
  [entities phone-id]
  (select entities
          (aggregate (count :id) :cnt :id)
          (where {:id phone-id})
          (limit 1)))

(defn logically-delete-record
  "Update record in specified table"
  [entities deleted-field-status field-status field-name field-value]
  (update entities
    (set-fields {:deleted deleted-field-status
                 :status field-status})
    (where {field-name [= field-value]})))

(defn update-record
  "Update record in specified table"
  [entities record-type phone deleted-status field-status field-name field-value]
  (update entities
    (set-fields {:phone phone
                 :record_type record-type
                 :deleted deleted-status
                 :status field-status})
    (where {field-name [= field-value]})))

(defn my-value-writer
  [key value]
  (if (= key :created_on)
    (str (java.sql.Date. (.getTime value)))
    value))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :record_id [[v/required :message "Record ID is required"] [v/integer :message "Record ID can only have integer value"]]
    :record_type [[v/required :message "Record Type is required"] [v/string :message "Record Type can only have alphabets"]]
    :phone [[v/required :message "Phone is required"] [v/number :message "Phone can only have numbers"]]))

(defn validate-phone-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :record_type [[v/required :message "Record Type is required"] [v/string :message "Record Type can only have alphabets"]]
    :phone [[v/required :message "Phone is required"] [v/number :message "Phone can only have numbers"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-phone
  "Add new phone number for a particular user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "phones" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (:record_id json-params)})
                    (let [usr (or (first (common-fn/select-join-record user-entities/user where-clause)))]

                      ;; If user exists; send login success
                      (if-not (empty? usr)
                        (do
                          ;;Insert into "address" table
                          (common-fn/insert-record contact-entities/phone {:record_id (:record_id json-params) :record_type (:record_type json-params) :phone (:phone json-params) :status "active"})

                          ;;Send "User created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn update-phone
  "Update record of selected user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "phones" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-phone-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:phone-id path-params))})
                    (let [usr (or (first (common-fn/select-record-count contact-entities/phone (common-fn/parse-number where-clause))))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? usr)
                        (do
                          (def set-fields-clause {:phone (:phone json-params) :record_type (:record_type json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:phone-id path-params))})

                          ;;Update record
                          (common-fn/update-record contact-entities/phone set-fields-clause where-clause)

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-phone
  "Get the record of selected address"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "phones" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [phone-id (get-in request [:path-params :phone-id])]
                phone-id (if (empty? phone-id) 0 phone-id)
                (def where-clause {:id (common-fn/parse-number phone-id)})
                (let [resultSet (or (first (common-fn/select-record contact-entities/phone where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-phones
  "Get all the records from the 'address' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "phones" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])]
                    row-offset (if (or (empty? row-offset)(nil? row-offset)) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))

                (let [resultSet (or (select contact-entities/phone
                                         (aggregate (count :id) :cnt :id)
                                         (where {:users.company_id company-id})
                                         (join 'users (= :users.id :record_id))
                                         (order :id :DESC)
                                         (limit row-limit)
                                         (offset row-offset)) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-phone
  "Delete record of a selected phone"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "phones" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [phone-id (get-in request [:path-params :phone-id])
                    phone-id (if (empty? phone-id) nil (common-fn/parse-number phone-id))]

                (def where-clause {:id phone-id})
                (let [usr (or (first (common-fn/select-record-count contact-entities/phone where-clause)) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? usr)
                    (do
                      ;;delete from "users" table
                      (common-fn/logically-delete-record contact-entities/phone "yes" "inactive" :id phone-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

