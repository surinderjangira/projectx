(ns ^{:doc "DB Entities for a Contact"
      :author "Surinder Singh Jangira"}
    contact.entities
    (:require [clojure.string :as str]
            [korma.core :as kc]
            [configuration.database :as db-config]
            [company.entities :as company-entities]))

(declare address email phone)

(kc/defentity address
  (kc/pk :id)
  (kc/table :address)
  (kc/entity-fields :id :record_type :record_id :address_1 :address_2 :city :state_id :country_id :zip :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to company-entities/company))

(kc/defentity email
  (kc/pk :id)
  (kc/table :email)
  (kc/entity-fields :id :record_type :record_id :email :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to company-entities/company)
  ;; mutations
  (kc/prepare (fn [{email :email :as v}]
               (if email
                 (assoc v :email (str/lower-case email)) v))))

(kc/defentity phone
  (kc/pk :id)
  (kc/table :phone)
  (kc/entity-fields :id :record_type :record_id :phone :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to company-entities/company))
