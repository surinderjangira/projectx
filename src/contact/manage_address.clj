(ns contact.manage_address
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [contact.entities :as contact-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :record_id [[v/required :message "Record ID is required"] [v/integer :message "Record ID can only have integer value"]]
    :record_type [[v/required :message "Record Type is required"] [v/string :message "Record Type can only have alphabets"]]
    :address_1 [[v/required :message "Address is required"] [v/string :message "Address can only have characters"]]
    :city [[v/required :message "City name is required"] [v/string :message "City name can only have alphabets"]]
    :state_id [[v/required :message "Please enter numeric value for State ID"] [v/integer :message "State ID can only have integer value"]]
    :country_id [[v/required :message "Please enter numeric value for Country ID"] [v/integer :message "Country ID can only have integer value"]]
    :zip [[v/required :message "Zip is required"] [v/string :message "Zip is required"]]))

(defn validate-address-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :record_type [[v/required :message "Record Type is required"] [v/string :message "Record Type can only have alphabets"]]
    :address_1 [[v/required :message "Address is required"] [v/string :message "Address can only have characters"]]
    :city [[v/required :message "City name is required"] [v/string :message "City name can only have alphabets"]]
    :state_id [[v/required :message "Please enter numeric value for State ID"] [v/integer :message "State ID can only have integer value"]]
    :country_id [[v/required :message "Please enter numeric value for Country ID"] [v/integer :message "Country ID can only have integer value"]]
    :zip [[v/required :message "Zip is required"] [v/string :message "Zip is required"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-address
  "Add new address for a particular user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "addresses" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (:record_id json-params)})
                    (let [usr (or (first (common-fn/select-join-record user-entities/user where-clause)))]

                      ;; If user exists; send login success
                      (if-not (empty? usr)
                        (do

                          ;;Insert into "address" table
                          (common-fn/insert-record contact-entities/address {:record_type (:record_type json-params) :record_id (:record_id json-params) :address_1 (:address_1 json-params) :address_2 (:address_2 json-params) :city (:city json-params) :state_id (:state_id json-params) :country_id (:country_id json-params) :zip (:zip json-params) :status "active"})

                          ;;Send "User created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn update-address
  "Update record of selected user"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "addresses" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-address-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:address-id path-params))})
                    (let [usr (or (first (common-fn/select-record-count contact-entities/address where-clause)))]

                      ;; If user doesn't exists; insert record
                      (if-not (empty? usr)
                        (do
                          (def set-fields-clause {:record_type (:record_type json-params) :address_1 (:address_1 json-params) :address_2 (:address_2 json-params) :city (:city json-params) :state_id (:state_id json-params) :country_id (:country_id json-params) :zip (:zip json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:address-id path-params))})

                          ;;Update record
                          (common-fn/update-record contact-entities/address set-fields-clause where-clause)

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-address
  "Get the record of selected address"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "addresses" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [address-id (get-in request [:path-params :address-id])]
                address-id (if (empty? address-id) 0 address-id)
                (def where-clause {:id (common-fn/parse-number address-id)})
                (let [resultSet (or (first (common-fn/select-record contact-entities/address where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-address
  "Get all the records from the 'address' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "addresses" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                    row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                    search-value (if (empty? search-value) nil search-value)

                ;;(def where-clause {:company_id company-id :adddress_1 ['ilike (str "%" search-value "%")] :status "active"})
                (let [resultSet (or (select contact-entities/address
                                         (aggregate (count :id) :cnt :id)
                                         (where {:status "active"
                                                 :users.company_id company-id})
                                         (where (or {:address_1 [ilike (str "%" search-value "%")]} {:address_2 [ilike (str "%" search-value "%")]} {:city [ilike (str "%" search-value "%")]}))
                                         (join 'users (= :users.id :record_id))
                                         (order :id :DESC)
                                         (limit row-limit)
                                         (offset row-offset)) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-address
  "Delete record of a selected address"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" "addresses" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [address-id (get-in request [:path-params :address-id])
                    address-id (if (empty? address-id) nil (common-fn/parse-number address-id))]
                (def where-clause {:id address-id})
                (let [usr (or (first (common-fn/select-record-count contact-entities/address where-clause)) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? usr)
                    (do
                      ;;delete from "users" table
                      (common-fn/logically-delete-record contact-entities/address "yes" "inactive" :id address-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

