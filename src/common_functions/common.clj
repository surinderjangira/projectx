(ns common_functions.common
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            ;;[ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            ;;[authorization.entities :as authorization-entities]
            ;;[bouncer.core :as b]
            ;;[bouncer.validators :as v]
            ;;[authorization.group_permissions :as auth-group-perm]
            ;;[login.authenticate :as auth-user]
            )
  (:use [korma.core]))

(defn parse-number
  "Reads a number from a string. Returns nil if not a number."
  [s]
  (if (re-find #"^-?\d+\.?\d*$" s)
    (read-string s)))

(defn my-value-writer
  "Date time converter"
  [key value]
  (if (= key :created_on)
    (str (java.sql.Date. (.getTime value)))
    value))

(defn sha1-str
  "Encrypt the string"
  [s]
  (->> (-> "sha1"
           java.security.MessageDigest/getInstance
           (.digest (.getBytes s)))
       (map #(.substring
              (Integer/toString
               (+ (bit-and % 0xff) 0x100) 16) 1))
       (apply str)))

(defn insert-record
  "Insert record in specified table"
  [entities field-values]
  (insert entities
    (values field-values)))

(defn update-record
  "Update user record in specified table"
  [entities set-fields-clause where-clause]
  (update entities
    (set-fields set-fields-clause)
    (where where-clause)))

(defn select-record
  "Select a record from a table"
  [entities where-clause]
  (select entities
    (where where-clause)
    (limit 1)))

(defn select-all-records
  "Select a record from a table"
  [entities where-clause]
  (select entities
    (where where-clause)))

(defn select-join-record
  "Select a record from a table"
  [entities where-clause]
  (select entities
    (where where-clause)
    (join 'email (= :email.record_id :id))
    (limit 1)))

(defn select-user-record-count
  "Select a record from a table"
  [entities where-clause]
  (select entities
    (aggregate (count :users.id) :cnt :id)
    (where where-clause)
    (join 'email (= :email.record_id :id))
    (limit 1)))

(defn select-record-count
  "Get the count of records"
  [entities where-clause]
  (select entities
    (aggregate (count :id) :cnt :id)
    (where where-clause)
    (limit 1)))

(defn select-record-with-list
  "Function used in List All Records functionality"
  [entities where-clause row-limit row-offset]
  (select entities
    (aggregate (count :id) :cnt :id)
    (where where-clause)
    (order :id :DESC)
    (limit row-limit)
    (offset row-offset)))

(defn select-record-with-join-list
  "Function used in List All Records functionality"
  [entities where-clause row-limit row-offset]
  (select entities
    (aggregate (count :id) :cnt :id)
    (where where-clause)
    (join 'users (= :users.id :record_id))
    (order :id :DESC)
    (limit row-limit)
    (offset row-offset)))

(defn logically-delete-record
  "logically delete record in specified table"
  [entities deleted-field-status field-status field-name field-value]
  (update entities
    (set-fields {:deleted deleted-field-status
                 :status field-status})
    (where {field-name [= field-value]})))
