(ns login.authenticate
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [ring.middleware.session.cookie :as cookie]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [contact.entities :as contact-entities]
            [authorization.entities :as authorization-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v])
  (:use [korma.core])
  (:import java.util.Base64))

(defn decode [to-decode]
  (String. (.decode (Base64/getDecoder) to-decode)))

(defn parse-number
  "Reads a number from a string. Returns nil if not a number."
  [s]
  (if (re-find #"^-?\d+\.?\d*$" s)
    (read-string s)))

(defn my-value-writer
  [key value]
  (if (= key :created_on)
    (str (java.sql.Date. (.getTime value)))
    value))

(defn sha1-str [s]
  (->> (-> "sha1"
           java.security.MessageDigest/getInstance
           (.digest (.getBytes s)))
       (map #(.substring
              (Integer/toString
               (+ (bit-and % 0xff) 0x100) 16) 1))
       (apply str)))

(defn select-record
  "Verify whether user already exists"
  [entities email password company-id]
  (select entities
          (fields :id :company_id :first_name :last_name)
          (where {:email.email email
                  :password password
                  :company_id company-id
                  :status "active"})
          (join 'email (= :email.record_id :id))
          (limit 1)))

(defn select-user-record
  "Verify whether user already exists"
  [entities email password company-id]
  (select entities
          (fields :id :user_group.group_name :company.name :access.access_type :first_name :last_name)
          (where {:email.email email
                  :password password
                  :company_id company-id
                  :status "active"})
          (join 'email (= :email.record_id :id))
          (join 'company (= :users.company_id :company.id))
          (join 'user_group (and (= :user_group.company_id :company.id) (= :user_group.id :users.group_id)))
          (join 'access (and (= :access.user_id :users.id) (= :access.type_id :users.company_id) (= :access.group_id :users.group_id)))
          (limit 1)))

(defn get-user-group
  "Get users group name"
  [entities user-id company-id group-id]
  (select entities
          (fields [:user_group.group_name :name])
          (where {:users.company_id company-id
                  :user_group.id group-id
                  :users.id user-id})
          (join 'user_group (= :users.group_id :user_group.id))
          (limit 1)))

(defn get-group-permissions
  "Get all permissions and access for the user"
  [entities company-id group-id]
  (select entities
          (fields :permission :access_level)
          (where {:company_id company-id
                  :group_id group-id})))

(defn get-user-permissions
  [resultset user-id company-id group-id]
  (let [group-resultset (first (get-user-group user-entities/user (:id resultset) (:company_id resultset) (:group_id resultset)))]
    (def group-name (:group_name group-resultset))
    (if (= group-name "suadmin")
              (do
                (def json-values {:authorization "full"})
                (ring-resp/response {:status 200 :body (json/write-str json-values)}))
              (do
                (let [resultSet (get-group-permissions authorization-entities/group_permission (:company_id resultset) (:group_id resultset))]
                  (def json-values {:authorization resultSet :user-id (:id resultset) :company-id (:company_id resultset) :group-id (:group_id resultset) :first-name (:first_name resultset)}))
                  (ring-resp/response {:status 200 :body (json/write-str json-values :value-fn my-value-writer)})))))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :email [[v/required :message "Email is required"] [v/email :message "Enter a valid email address"]]
    :password [[v/required :message "Password is required"] [v/min-count 8 :message "Password should be minimum 8 characters in length"]]))

;; query the database
(defn authenticate-user-token
  "Authenticate user token"
  [headers]
  (let [email (get (str/split (decode (get (str/split (get headers "authorization") #" ") 1)) #":") 0)
        password (get (str/split (decode (get (str/split (get headers "authorization") #" ") 1)) #":") 1)
        company-id (parse-number (get headers "x-company-id"))]

    (let [user-resultset (or (first (select-record user-entities/user (str/trim email) (sha1-str (str/trim password)) company-id)))]

      ;; If user exists; send login success
      (if-not (empty? user-resultset)
        (do
          (def json-values {:group-id (:group_id user-resultset) :company-id company-id})
          (ring-resp/response (json/write-str json-values)))
        (do

          (def json-values {:group-id 0})
          (ring-resp/response (json/write-str json-values)))))))

;; query the database
(defn authenticate-user
  "Authenticate user"
  [email password company-id request]

  (let [user-resultset (or (first (select-user-record user-entities/user (str/trim email) (str/trim password) company-id)))]

    ;; If user exists; send login success
    (if-not (empty? user-resultset)
      (do
        (let [user-id (:id user-resultset)]

          (def authorization (get (:headers request) "authorization"))

           ;; generate Token
          (def json-values {:message "ok"
                            :user-id (:id user-resultset)
                            :access-type (:access_type user-resultset)
                            :company-id (:company_id user-resultset)
                            :company-name (:name user-resultset)
                            :group-id (:group_id user-resultset)
                            :group-name (:group_name user-resultset)
                            :first-name (:first_name user-resultset)
                            :token authorization})
          (ring-resp/response {:status 200 :body (json/write-str json-values)})))
      (do
        ;;Send "Invalid Credentials / User does not exists" response
        (ring-resp/response {:status 401 :body "invalid credentials/user does not exists."})))))

(defn authenticate-me
  [{:keys [headers params json-params path-params] :as request}]

  ;;Validate data
    (let [errors (validate-data json-params)]

      ;;If "error" found; return error message
      (if-not (= (get errors 0) nil)
        (do
          (ring-resp/response {:status 400 :body (json/write-str errors)}))
        (do
          ;; Query the DB
          (authenticate-user (:email json-params) (sha1-str (:password json-params)) (parse-number (get headers "x-company-id")) request)))))
