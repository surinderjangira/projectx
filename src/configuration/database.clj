(ns configuration.database
  (:require [ring.util.response :as ring-resp]
            [korma.db]
            [clojure.java.jdbc :as sql]))

;; Or without predefining a connection map:
(korma.db/defdb projectxdb (korma.db/postgres {:db "projectx"}))

(let [db-host "localhost"
      db-port 5432
      db-name "projectx"]

  (def db-conn {:classname "org.postgresql.Driver" ; must be in classpath
           :subprotocol "postgresql"
           :subname (str "//" db-host ":" db-port "/" db-name)
           ; Any additional keys are passed to the driver
           ; as driver-specific properties.
           ;;:user ""
           ;;:password ""
                }))
