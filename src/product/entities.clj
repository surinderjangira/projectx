(ns ^{:doc "DB Entities for an User"
      :author "Surinder Singh Jangira"}
    product.entities
    (:require [clojure.string :as str]
              [korma.core :as kc]
              [configuration.database :as db-config]
              [authorization.entities :as authorization-entities]
              [company.entities :as company-entities]))

(declare category category_attribute product product_option product_attribute)

(kc/defentity category
  (kc/pk :id)
  (kc/table :category)
  (kc/entity-fields :id :company_id :parent_id :category_name :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to company-entities/company)
  (kc/has-many category_attribute))

(kc/defentity category_attribute
  (kc/pk :id)
  (kc/table :category_attribute)
  (kc/entity-fields :id :company_id :category_id :attribute_name :attribute_type :attribute_options :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to category))

(kc/defentity product
  (kc/pk :id)
  (kc/table :product)
  (kc/entity-fields :id :company_id :category_id :product_name :sku :upc :asin :price :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to company-entities/company)
  (kc/belongs-to category)
  (kc/has-many product_attribute))

(kc/defentity product_option
  (kc/pk :id)
  (kc/table :product_option)
  (kc/entity-fields :id :name :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/has-many product_attribute))

(kc/defentity product_attribute
  (kc/pk :id)
  (kc/table :product_attribute)
  (kc/entity-fields :id :product_id :company_id :product_option_id :attribute_values :deleted :status :created_on)
  (kc/database db-config/projectxdb)
  ;; relationships
  (kc/belongs-to product)
  (kc/has-many product_option))
