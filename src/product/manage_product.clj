(ns product.manage_product
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [product.entities :as product-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :category_id [[v/required :message "Category ID is required"] [v/integer :message "Category ID can only have integer value"]]
    :product_name [[v/required :message "Product name is required"] [v/string :message "Product name can only have characters"]]
    ;;:price [[v/required :message "Price is required"] [v/integer :message "Price can only have numeric values"]]
              ))

(defn validate-product-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :category_id [[v/required :message "Category ID is required"] [v/integer :message "Category ID can only have integer value"]]
    :product_name [[v/required :message "Product name is required"] [v/string :message "Product name can only have characters"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-product
  "Add new product for a particular category"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "products" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:company_id (:company_id json-params) :category_id (:category_id json-params) :product_name (:product_name json-params)})
                    (let [products (or (first (common-fn/select-record product-entities/product where-clause)))]

                      ;; If user exists; send login success
                      (if (empty? products)
                        (do

                          ;;Insert into "product" table
                          (common-fn/insert-record product-entities/product {:company_id (:company_id json-params) :category_id (:category_id json-params) :product_name (:product_name json-params) :sku (:sku json-params) :upc (:upc json-params) :asin (:asin json-params) :price (:price json-params) :status (:status json-params)})

                          ;;Send "Category created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "Product already exists" response
                          (ring-resp/response {:status 409 :body "{message:\"record already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))


(defn update-product
  "Update record of selected product"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "products" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-product-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:product-id path-params)) :company_id (common-fn/parse-number (get headers "x-company-id"))})
                    (let [products (or (first (common-fn/select-record-count product-entities/category where-clause)))]

                      ;; If product exists; update record
                      (if-not (empty? products)
                        (do
                          (def set-fields-clause {:category_id (:category_id json-params) :product_name (:product_name json-params) :sku (:sku json-params) :upc (:upc json-params) :asin (:asin json-params) :price (:price json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:product-id path-params))})

                          ;;Update record
                          (common-fn/update-record product-entities/product set-fields-clause where-clause)

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-product
  "Get the record of selected product"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "products" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [product-id (get-in request [:path-params :product-id])]
                product-id (if (empty? product-id) 0 product-id)
                (def where-clause {:id (common-fn/parse-number product-id)})
                (let [resultSet (or (first (common-fn/select-record product-entities/product where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-products
  "Get all the records from the 'product' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "products" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                    row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                    search-value (if (empty? search-value) nil search-value)

                ;;(def where-clause {:company_id company-id :adddress_1 ['ilike (str "%" search-value "%")] :status "active"})
                (let [resultSet (or (select product-entities/product
                                         (aggregate (count :id) :cnt :id)
                                         (where {:status "active"
                                                 :company_id company-id})
                                         (where {:product_name [ilike (str "%" search-value "%")]})
                                         (order :id :DESC)
                                         (limit row-limit)
                                         (offset row-offset)) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
