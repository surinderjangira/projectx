(ns product.manage_product_attributes
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [product.entities :as product-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn]
            [configuration.database :as db-config])
  (:use [korma.core]
        [clojure.java.jdbc :as sql]))

(def con (sql/get-connection db-config/db-conn))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :product_id [[v/required :message "Product ID is required"] [v/integer :message "Product ID can only have integer value"]]
    :product_option_id [[v/required :message "Product option ID is required"] [v/integer :message "Product option ID can only have integer value"]]
    ;;:product_name [[v/required :message "Product name is required"] [v/string :message "Product name can only have characters"]]
              ))

(defn validate-product-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :product_id [[v/required :message "Product ID is required"] [v/integer :message "Product ID can only have integer value"]]
    :product_option_id [[v/required :message "Product option ID is required"] [v/integer :message "Product option ID can only have integer value"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-product-attribute
  "Add new product for a particular category"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "prod-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:product_id (:product_id json-params) :company_id (common-fn/parse-number (get headers "x-company-id")) :product_option_id (:product_option_id json-params)})
                    (let [products (or (first (common-fn/select-record product-entities/product_attribute where-clause)))]

                      ;; If user exists; send login success
                      (if (empty? products)
                        (do

                          (def val-to-insert (.createArrayOf con "varchar" (into-array String (:attribute_values json-params))))
                          ;;Insert into "product" table
                          (common-fn/insert-record product-entities/product_attribute {:product_id (:product_id json-params) :company_id (common-fn/parse-number (get headers "x-company-id")) :product_option_id (:product_option_id json-params) :attribute_values val-to-insert :status "active"})
                          ;;(common-fn/insert-record product-entities/product_attribute {:product_id (:product_id json-params) :company_id (common-fn/parse-number (get headers "x-company-id")) :product_option_id (:product_option_id json-params) :status (:status json-params)})

                          ;;Send "Category created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "Product already exists" response
                          (ring-resp/response {:status 409 :body "{message:\"record already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))


(defn update-product-attribute
  "Update record of selected product attribute"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "prod-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-product-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:prod-attribute-id path-params)) :product_id (:product_id json-params)})
                    (let [products (or (first (common-fn/select-record-count product-entities/product_attribute where-clause)))]

                      ;; If product exists; update record
                      (if-not (empty? products)
                        (do

                          (def val-to-update (.createArrayOf con "varchar" (into-array String (:attribute_values json-params))))
                          (def set-fields-clause {:product_id (:product_id json-params) :product_option_id (:product_option_id json-params) :attribute_values val-to-update :deleted (:deleted_status json-params) :status (:status json-params)})
                          ;;(def set-fields-clause {:product_id (:product_id json-params) :product_option_id (:product_option_id json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:prod-attribute-id path-params))})

                          ;;Update record
                          (common-fn/update-record product-entities/product_attribute set-fields-clause where-clause)

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-product-attribute
  "Get the record of selected product attribute"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "prod-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [prod-attribute-id (get-in request [:path-params :prod-attribute-id])]
                prod-attribute-id (if (empty? prod-attribute-id) 0 prod-attribute-id)
                (def where-clause {:id (common-fn/parse-number prod-attribute-id)})
                (let [resultSet (or (first (common-fn/select-record product-entities/product_attribute where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-products-attribute
  "Get all the records from the 'product_attribute' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "prod-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                    row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                    search-value (if (empty? search-value) nil search-value)

                ;;(def where-clause {:company_id company-id :adddress_1 ['ilike (str "%" search-value "%")] :status "active"})
                (let [resultSet (or (select product-entities/product_attribute
                                         (aggregate (count :id) :cnt :id)
                                         (where {:company_id company-id})
                                         ;;(where {:product_name [ilike (str "%" search-value "%")]})
                                         (order :id :DESC)
                                         (limit row-limit)
                                         (offset row-offset)) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-product-attribute
  "Delete record of a selected product attribute"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" "prod-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [prod-attribute-id (get-in request [:path-params :prod-attribute-id])
                    prod-attribute-id (if (empty? prod-attribute-id) nil (common-fn/parse-number prod-attribute-id))]
                (def where-clause {:id prod-attribute-id})
                (let [usr (or (first (common-fn/select-record-count product-entities/product_attribute where-clause)) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? usr)
                    (do
                      ;;delete from "users" table
                      (common-fn/logically-delete-record product-entities/product_attribute "yes" "inactive" :id prod-attribute-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
