(ns product.manage_category
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [product.entities :as product-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn])
  (:use [korma.core]))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :category_name [[v/required :message "Category name is required"] [v/string :message "Category name can only have characters"]]))

(defn validate-category-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :category_name [[v/required :message "Category name is required"] [v/string :message "Category name can only have characters"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-category
  "Add new category for a particular comapny"
  [{:keys [headers params json-params path-params] :as request}]

  ;;(get-parent-category 1)

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "categories" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:company_id (:company_id json-params) :parent_id (:parent_id json-params) :category_name (:category_name json-params)})
                    (let [category (or (first (common-fn/select-record product-entities/category where-clause)))]

                      ;; If user exists; send login success
                      (if (empty? category)
                        (do

                          ;;Insert into "category" table
                          (common-fn/insert-record product-entities/category {:company_id (:company_id json-params) :parent_id (:parent_id json-params) :category_name (:category_name json-params) :status "active"})

                          ;;Send "Category created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "Category already exists" response
                          (ring-resp/response {:status 409 :body "{message:\"record already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))


(defn update-category
  "Update record of selected category"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "categories" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-category-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:category-id path-params)) :company_id (common-fn/parse-number (get headers "x-company-id"))})
                    (let [category (or (first (common-fn/select-record-count product-entities/category where-clause)))]

                      ;; If category exists; update record
                      (if-not (empty? category)
                        (do
                          (def set-fields-clause {:parent_id (:parent_id json-params) :category_name (:category_name json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:category-id path-params))})

                          ;;Update record
                          (common-fn/update-record product-entities/category set-fields-clause where-clause)

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-category
  "Get the record of selected category"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "categories" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [category-id (get-in request [:path-params :category-id])]
                category-id (if (empty? category-id) 0 category-id)
                (def where-clause {:id (common-fn/parse-number category-id)})
                (let [resultSet (or (first (common-fn/select-record product-entities/category where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-parent-category
  "Get the record of selected category"
  [company-id]

  (dry-run (select product-entities/category
                  (aggregate (count :id) :cnt :id)
                  (where {:company_id company-id})
                  (order :id :DESC)))

  (let [resultSet (or (first (common-fn/select-record product-entities/category where-clause)) nil)]

      ;; If records exists;
      (if-not (empty? resultSet)
        (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
        (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))
  )

(defn list-all-categories
  "Get all the records from the 'category' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "categories" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                    row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                    search-value (if (empty? search-value) nil search-value)

                ;;(def where-clause {:company_id company-id :adddress_1 ['ilike (str "%" search-value "%")] :status "active"})
                (let [resultSet (or (select product-entities/category
                                         (aggregate (count :id) :cnt :id)
                                         (where {:status "active"
                                                 :company_id company-id})
                                         (where {:category_name [ilike (str "%" search-value "%")]})
                                         (order :id :DESC)
                                         (limit row-limit)
                                         (offset row-offset)) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-category
  "Delete record of a selected vendor"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" "categories" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do
                (let [category-id (get-in request [:path-params :category-id])
                    category-id (if (empty? category-id) nil (common-fn/parse-number category-id))]
                  (def where-clause {:id category-id})
                  (let [category-result (or (common-fn/select-record product-entities/category where-clause) nil)]

                    ;; If user exists; send login success
                    (if-not (empty? category-result)
                      (do
                        (def where-clause2 {:category_id category-id :status "active"})
                        (let [product-result (or (common-fn/select-record-count product-entities/product where-clause2) nil)]
                          (if-not (empty? product-result)
                            (do

                              ;;Delete from "category" table
                              (common-fn/logically-delete-record product-entities/category "yes" "inactive" :id category-id)

                              ;;Delete from "category_attribute" table
                              (common-fn/logically-delete-record product-entities/category_attribute "yes" "inactive" :category_id category-id)

                              ;;Delete from "product" table
                              (common-fn/logically-delete-record product-entities/product "yes" "inactive" :category_id category-id)

                              ;;Send "Product deleted" response
                              (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                            (do

                              ;;Send "Vendor not found" response
                              (ring-resp/response {:status 204 :body "{message:\"no content / already deleted\"}"})))))
                      (do
                        ;;Send "Vendor not found" response
                        (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

