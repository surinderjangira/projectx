(ns product.manage_category_attributes
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [user.entities :as user-entities]
            [product.entities :as product-entities]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [authorization.group_permissions :as auth-group-perm]
            [login.authenticate :as auth-user]
            [common_functions.common :as common-fn]
            [configuration.database :as db-config])
  (:use [korma.core]
        [clojure.java.jdbc :as sql]))

(def con (sql/get-connection db-config/db-conn))

(defn validate-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :company_id [[v/required :message "Company ID is required"] [v/integer :message "Company ID can only have integer value"]]
    :category_id [[v/required :message "Category ID is required"] [v/integer :message "Category ID can only have integer value"]]
    :attribute_name [[v/required :message "Attribute name is required"] [v/string :message "Attribute name can only have characters"]]
    :attribute_type [[v/required :message "Attribute type is required"] [v/string :message "Attribute type can only have characters"]]
    ;;:attribute_options [[v/required :message "Attribute options is required"] [v/string :message "Attribute options can only have characters"]]
              ))

(defn validate-category-data
  "Validate the input data"
  [json-params]
  (b/validate json-params
    :category_id [[v/required :message "Category ID is required"] [v/integer :message "Category ID can only have integer value"]]
    :attribute_name [[v/required :message "Attribute name is required"] [v/string :message "Attribute name can only have characters"]]
    :attribute_type [[v/required :message "Attribute type is required"] [v/string :message "Attribute type can only have characters"]]
;;:attribute_options [[v/required :message "Attribute options is required"] [v/string :message "Attribute options can only have characters"]]
    :deleted_status [[v/required :message "Deleted Status is required"] [v/string :message "Deleted Status can only have alphabets"]]
    :status [[v/required :message "Status is required"] [v/string :message "Status can only have alphabets"]]))

(defn insert-category-attribute
  "Add new category for a particular comapny"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "create" "cat-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:company_id (:company_id json-params) :category_id (:category_id json-params) :attribute_name (:attribute_name json-params)})
                    (let [category-attri (or (first (common-fn/select-record product-entities/category_attribute where-clause)))]

                      ;; If user exists; send login success
                      (if (empty? category-attri)
                        (do

                          (def val-to-insert (.createArrayOf con "varchar" (into-array String (:attribute_options json-params))))

                          ;;Insert into "category_attribute" table
                          (common-fn/insert-record product-entities/category_attribute {:company_id (:company_id json-params) :category_id (:category_id json-params) :attribute_name (:attribute_name json-params) :attribute_type (:attribute_type json-params) :attribute_options val-to-insert :status "active"})
                          ;;(common-fn/insert-record product-entities/category_attribute {:company_id (:company_id json-params) :category_id (:category_id json-params) :attribute_name (:attribute_name json-params) :attribute_type (:attribute_type json-params) :status "active"})

                          ;;Send "Category Attribute created" response
                          (ring-resp/response {:status 201 :body "{created:\"ok\"}"}))
                        (do
                          ;;Send "Category already exists" response
                          (ring-resp/response {:status 409 :body "{message:\"record already exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))


(defn update-category-attribute
  "Update record of selected category attribute"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "update" "cat-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              ;;Validate data
              (let [errors (validate-category-data json-params)]

                ;;If "error" found; return error message
                (if-not (= (get errors 0) nil)
                  (do
                    (ring-resp/response {:status 400 :body (json/write-str errors)}))
                  (do
                    (def where-clause {:id (common-fn/parse-number (:cat-attribute-id path-params)) :company_id (common-fn/parse-number (get headers "x-company-id"))})
                    (let [category-attribute (or (first (common-fn/select-record-count product-entities/category_attribute where-clause)))]

                      ;; If category exists; update record
                      (if-not (empty? category-attribute)
                        (do

                          (def val-to-update (.createArrayOf con "varchar" (into-array String (:attribute_options json-params))))

                          (def set-fields-clause {:attribute_name (:attribute_name json-params) :attribute_type (:attribute_type json-params) :attribute_options val-to-update :deleted (:deleted_status json-params) :status (:status json-params)})
                          ;;(def set-fields-clause {:attribute_name (:attribute_name json-params) :attribute_type (:attribute_type json-params) :deleted (:deleted_status json-params) :status (:status json-params)})
                          (def where-clause {:id (common-fn/parse-number (:cat-attribute-id path-params))})

                          ;;Update record
                          (common-fn/update-record product-entities/category_attribute set-fields-clause where-clause)

                          ;;Send "User updated" response
                          (ring-resp/response {:status 200 :body "{updated:\"ok\"}"}))
                        (do
                          ;;Send "User already exists" response
                          (ring-resp/response {:status 409 :body "{conflict:\"record does not exists\"}"}))))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn get-selected-category-attribute
  "Get the record of selected category attributes"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "cat-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [category-attribute-id (get-in request [:path-params :cat-attribute-id])]
                category-attribute-id (if (empty? category-attribute-id) 0 category-attribute-id)
                (def where-clause {:id (common-fn/parse-number category-attribute-id)})
                (let [resultSet (or (first (common-fn/select-record product-entities/category_attribute where-clause)) nil)]

                  ;; If records exists;
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn list-all-category-attributes
  "Get all the records from the 'category' table"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "read" "cat-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [row-offset (get-in request [:query-params :offset])
                    row-limit (get-in request [:query-params :limit])
                    search-value (get-in request [:query-params :q])]
                    row-offset (if (empty? row-offset) 0 (common-fn/parse-number row-offset))
                    row-limit (if (empty? row-limit) 1 (common-fn/parse-number row-limit))
                    search-value (if (empty? search-value) nil search-value)

                ;;(def where-clause {:company_id company-id :adddress_1 ['ilike (str "%" search-value "%")] :status "active"})
                (let [resultSet (or (select product-entities/category_attribute
                                         (aggregate (count :id) :cnt :id)
                                         (where {:status "active"
                                                 :company_id company-id})
                                         (where (or {:attribute_name [ilike (str "%" search-value "%")]} {:attribute_type [ilike (str "%" search-value "%")]}))
                                         (order :id :DESC)
                                         (limit row-limit)
                                         (offset row-offset)) nil)]
                  ;; If user doesn't exists; insert record
                  (if-not (empty? resultSet)
                    (ring-resp/response (json/write-str resultSet :value-fn common-fn/my-value-writer))
                    (ring-resp/response {:status 204 :body "{message:\"no content\"}"})))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))

(defn delete-category-attribute
  "Delete record of a selected category attribute"
  [{:keys [headers params json-params path-params] :as request}]

  ;;Check whether "token" is a valid base64_encoded string

  (let [group-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "group-id" )
        company-id (get (json/read-str (get-in (auth-user/authenticate-user-token headers) [:body])) "company-id" )]
    (if (> group-id 0)
      (do

        ;;Check whether the logged-in user has permission to Update the Group Permissions
        (let [check-auth (auth-group-perm/check-authorization "delete" "cat-attributes" group-id)]

          (if (= (get-in check-auth [:body :authorize]) true)
            (do

              (let [cat-attribute-id (get-in request [:path-params :cat-attribute-id])
                    cat-attribute-id (if (empty? cat-attribute-id) nil (common-fn/parse-number cat-attribute-id))]
                (def where-clause {:id cat-attribute-id})
                (let [usr (or (first (common-fn/select-record-count product-entities/category_attribute where-clause)) nil)]

                  ;; If user exists; send login success
                  (if-not (empty? usr)
                    (do
                      ;;delete from "users" table
                      (common-fn/logically-delete-record product-entities/category_attribute "yes" "inactive" :id cat-attribute-id)

                      ;;Send "User created" response
                      (ring-resp/response {:status 201 :body "{deleted:\"ok\"}"}))
                    (do
                      ;;Send "User already exists" response
                      (ring-resp/response {:status 204 :body "{message:\"no content\"}"}))))))
            (do
                (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
      (do
        (ring-resp/response {:status 401 :body "{status:\"Unauthorized access\"}"})))))
